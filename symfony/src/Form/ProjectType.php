<?php

namespace App\Form;

use App\Entity\Course;
use App\Entity\Project;
use App\Entity\Student;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProjectType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('maxStudentCount')
            ->add('grades')
            ->add('student', EntityType::class, [
                'class' => Student::class,
                'multiple' => true,
                'expanded' => true
            ])
            ->add('course', EntityType::class, [
                'class' => Course::class,
                'choice_label' => function($course) {
                    return $course->getName();
                }
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Project::class,
        ]);
    }
}
