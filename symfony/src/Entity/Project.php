<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProjectRepository")
 */
class Project
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="integer")
     */
    private $maxStudentCount;

    /**
     * @ORM\Column(type="integer")
     * @Assert\Range(
     *      min = 0,
     *      max = 20,
     *      minMessage = "note comprise entre 0 et 20",
     *      maxMessage = "note comprise entre 0 et 20"
     * )
     */
    private $grades;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Student", inversedBy="project")
     */
    private $student;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Course", inversedBy="project", cascade={"persist", "remove"})
     */
    private $course;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getMaxStudentCount(): ?int
    {
        return $this->maxStudentCount;
    }

    public function setMaxStudentCount(int $maxStudentCount): self
    {
        $this->maxStudentCount = $maxStudentCount;

        return $this;
    }

    public function getGrades(): ?int
    {
        return $this->grades;
    }

    public function setGrades(int $grades): self
    {
        $this->grades = $grades;

        return $this;
    }

    public function getStudent(): ?Student
    {
        return $this->student;
    }

    public function setStudent(?Student $student): self
    {
        $this->student = $student;

        return $this;
    }

    public function getCourse(): ?course
    {
        return $this->course;
    }

    public function setCourse(?course $course): self
    {
        $this->course = $course;

        return $this;
    }
}
