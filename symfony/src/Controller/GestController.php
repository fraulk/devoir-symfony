<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class GestController extends AbstractController
{
    /**
     * @Route("/", name="gest")
     */
    public function index()
    {
        return $this->render('gest/index.html.twig', [
            'controller_name' => 'GestController',
        ]);
    }
}
